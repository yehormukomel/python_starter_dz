"""
Завдання 1
"""

a = int(input('Введіть число 1: '))
b = int(input("Введіть число 2: "))

# Варіант із циклом
sum_of_numbers = 0

for i in range(a, b+1):
    sum_of_numbers += i

print(sum_of_numbers)

# Варіант через sum
print(sum(range(a, b+1)))


"""
Завдання 2
"""

n = int(input("Введіть число: "))+1
factorial = 1
for i in range(2, n):
    factorial *= i

print(factorial)

"""
Завдання 3
"""

val = int(input('Сторона трикутника: '))+1
for i in range(1, val):
    for _ in range(i):
        print('*', end='')
        print(' ', end='')
    print()


"""
Завдання 4
"""

a = int(input("Введіть a: "))
b = int(input("Введіть b: "))

avg = (a + b) / 2

for i in range(a, b+1):
    if i % avg == 0:
        print(i)

"""
Завдання 5
"""

height = int(input("Введіть висоту: "))
width = int(input("Введіть ширину: "))

for i in range(height):
    print('* '*width)

"""
Завдання 6
"""

login = '111'
password = '222'
counter = 0

while counter < 3:
    counter += 1

    print(f"Спроба: {counter}")

    user_login = input("Введіть логін: ")
    user_password = input("Введіть пароль: ")

    if user_login == login and user_password == password:
        print(f"Авторизацію успішно пройдено з {counter} спроби")
        break
    else:
        print("Невірно введений логін чи пароль!")
        print()
        continue
else:
    print("Більше немає спроб :(")

