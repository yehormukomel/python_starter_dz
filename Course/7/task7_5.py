from collections import Counter, defaultdict
# from exampletext import text

text = input("Введіть рядок в якому багато слів: ")

"""
Варіант через Counter
"""
counter = Counter(text.lower().split())
# print(counter)
print(dict(counter))


"""
Варіант з defaultdict
"""
bad_symbols = ("-", "_", ",", "$", "#", "@", "^", "&", "%", "*", "+", "=", "/", ".")
a_dict = defaultdict(int)
for char in text.lower().split():
    char = char.strip()
    if char not in bad_symbols:
        a_dict[char] += 1

print()
print(sorted(a_dict.items(), key=lambda x: x[1], reverse=True))
