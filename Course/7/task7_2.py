"""
Завдання 2

Створіть програму, яка емулює роботу сервісу зі скорочення посилань.
Повинна бути реалізована можливість введення початкового посилання
та короткої назви і отримання початкового посилання за її назвою.
"""


def get_ui_val(q_string: str, var_count: int):
    variants_count = [str(i) for i in range(1, var_count + 1)]
    print(q_string)
    ui = input(">> ")
    while ui not in variants_count:
        print("Потрібно ", end='')
        for index, item in enumerate(variants_count):
            print(f"{item} {'або' if index != len(variants_count) - 1 else '!'}", end=' ')
        print()

        ui = input(">> ")
    return ui


def get_link():
    website_link = input("Введіть посилання: ")
    while len(website_link.split('.')) < 2:
        print("Введіть будь ласка адресу сайту! Повинна бути крапка в посиланні!")
        website_link = input("Введіть посилання: ")
    return website_link


if __name__ == '__main__':
    question_string = "Чого бажаєте?\n1. Добавити нове посилання\n2. Знайти існуюче посилання за назвою\n3. Вихід"
    names_and_shortnames = dict()
    while True:
        ui = get_ui_val(question_string, 3)

        if ui == "1":
            website_link = get_link()
            short_name = input("Введіть коротку назву для цього посилання: ")
            names_and_shortnames[short_name] = website_link
            print("Посилання було успішно записано!")
            print()
        elif ui == "2":
            short_name = input("Введіть коротку назву посилання: ")
            website_link = names_and_shortnames.get(short_name)
            if website_link:
                print(f"Коротка назва: {short_name}\nОсь повне посилання: {website_link}")
                print()
            else:
                print("Посилання за такою назвою не було знайдено :(")
                print()
        else:
            print("До побачення!")
            break
