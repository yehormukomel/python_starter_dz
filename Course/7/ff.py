from colorama import Fore
from colorama import Style
from prettytable import PrettyTable

database = {
    'Шевченко': ['Technical \nProject Manager', 20, 'ABCloudZ', 100, 'software development life cycle, \nexperience working \nwith Agile or Scrum', 7000 ],
    'Франко': ['web developer', 5, 'Інтернет магазин', 85, 'Prestashop, Wordpress, \nOpencart, Drupal', 4000 ],
    'Українка': ['DevOps', 3, 'Підтримка мережі офісу', 55, 'Inno, Setup , NSIS', 2500 ],
    'Мирний': ['Python Developer', 1, 'Інтернет магазин, чат-бот', 50, 'SQL, Backend', 1000 ],
    'Котляревський': ['PHP Developer', 10, 'CRM for The National \nPolice of Ukraine', 90, 'php, js, mysql', 5000 ],
    'Коцюбинський': ['С#/.NET Developer', 15, 'System Integration with C#', 98, 'MS SQL, SQL, SSIS, C# .NET', 6500 ]
}

def menu_edit(full_name):

    operator1 = 0
    while operator1 != 'menu':
        operator1 = input("Виберіть операцію: для зміни посади введіть                   '1'\n\
                           для зміни досвіду введіть          '2'\n\
                           для зміни портфоліо                '3'\n\
                           для зміни коефіцієнта ефективності '4'\n\
                           для зміни  стеку технологій        '5'\n\
                           для зміни зарплати                 '6'\n\
                           для виходу в головне меню     'menu' : ")
        if operator1 == '1':
                job_title = input('Введіть посаду : ')
                database[full_name] = [job_title, database[full_name][1], database[full_name][2],
                                       database[full_name][3], database[full_name][4], database[full_name][5]]
        elif operator1 == '2':
                experience = int(input('Введіть досвід роботи в роках : '))
                database[full_name] = [database[full_name][0], experience, database[full_name][2],
                                       database[full_name][3], database[full_name][4], database[full_name][5]]
        elif operator1 == '3':
                portfolio = input('Введіть портфоліо : ')
                database[full_name] = [database[full_name][0], database[full_name][1], portfolio,
                                       database[full_name][3], database[full_name][4], database[full_name][5]]
        elif operator1 == '4':
                efficiency_ratio = int(input('Введіть значення коефіцієнту ефективності : '))
                database[full_name] = [database[full_name][0], database[full_name][1], database[full_name][2],
                                       efficiency_ratio, database[full_name][4], database[full_name][5]]
        elif operator1 == '5':
                technology_stack = input('Введіть стек технологій : ')
                database[full_name] = [database[full_name][0], database[full_name][1], database[full_name][2],
                                       database[full_name][3], technology_stack, database[full_name][5]]
        elif operator1 == '6':
                salary = int(input('Введіть розмір зарплати : '))
                database[full_name] = [database[full_name][0], database[full_name][1], database[full_name][2],
                                       database[full_name][3], database[full_name][4], salary]
        elif operator1 == 'menu':
                print('перехід в головне меню')
        else:
            print('Не вірний вибір')

def hr():
    operator = 0
    while operator not in ('1', '2', '3', '4', 'exit'):
        operator = input("Робота з базою обліку кадрів.\n\
            Виберіть операцію: для додавання нового робітника введіть '1'\n\
                               для внесення змін в дані робітника '2'\n\
                               для перегляду списку за прізвищами '3'\n\
                               для перегляду списку за коефіцієнтам ефективності '4'\n\
                               для виходу - 'exit' : ")
    match operator:
        case '1':
            full_name = input('Введіть прізвище робітника : ')
            job_title = input('Введіть посаду : ')
            experience = int(input('Введіть досвід роботи в роках : '))
            portfolio = input('Введіть портфоліо : ')
            efficiency_ratio = int(input('Введіть значення коефіцієнту ефективності : '))
            technology_stack = input('Введіть стек технологій : ')
            salary = int(input('Введіть розмір зарплати : '))
            database[full_name] = [job_title, experience, portfolio, efficiency_ratio, technology_stack, salary]
        case '2':
            data_item = list(database.keys())
            full_name = input('Введіть прізвище робітника : ')
            if full_name in data_item:
                menu_edit(full_name)
            else:
                print('Прізвище введено не вірно')
        case '3':
            print(Fore.BLUE + 'Список робітників, сортований за прізвищами')
            data_item = database.items()
            table = PrettyTable(['прізвище', 'посада', 'досвід', 'портфоліо', 'коефіцієнт ефективності', 'стек технологій', 'зарплата'])
            for a, b in data_item:
                table.add_row([a, b[0], b[1], b[2], b[3], b[4], b[5]])
            print(table)
            print(Style.RESET_ALL)
        case '4':
            print(Fore.BLUE + 'Список робітників, сортований за коефіцієнтами ефективності')
            data_value = sorted(database.items(), key=lambda x: x[1][3], reverse=True)
            table = PrettyTable(['прізвище', 'посада', 'досвід', 'портфоліо', 'коефіцієнт ефективності', 'стек технологій', 'зарплата'])
            for k, v in data_value:
                table.add_row([k, v[0], v[1], v[2], v[3], v[4], v[5]])
            print(table)
            print(Style.RESET_ALL)
        case 'exit':
            print('Завершення програми')
            exit()

operator = 0
while operator != 'exit':
    hr()