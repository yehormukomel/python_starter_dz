from collections import defaultdict
from task7_2 import get_ui_val

"""
Завдання 6

Створіть прототип програми «Бібліотека»,
де є можливість перегляду та внесення змін за структурою:
автор: твір. Передбачте можливість виведення на екран
сортування за автором та твором.
"""

"""
Я використовую функцію get_ui_val з іншого дз, сподіваюся це норм
"""


def print_names(k, lib):
    print(f"автор: {k}")
    print("Твори: ")
    for i in lib[k]:
        print(i, end='; ')
    print()
    print()


def sort_and_print_vals(lib, num):
    if not lib:
        print("Бібліотека пуста!")

    if num == '1':
        print("Сортування за автором: ")
        for k, v in sorted(lib.items(), key=lambda x: x[0]):
            print_names(k, lib)

    elif num == '2':
        print("Сортування за творами: ")
        # list сортується теж в лексикографічному порядку
        # потім словник по list'у
        for k, v in sorted(lib.items(), key=lambda x: (sorted(x[1]))):
            print_names(k, lib)

    elif num == '3':
        for k, v in lib.items():
            print_names(k, lib)


def change_author(lib):
    for k, v in lib.items():
        print_names(k, lib)

    print("Введіть автора, ім'я якого ви хочете змінити: ")
    author_name = input(">> ")
    books_list = lib.get(author_name)
    if not books_list:
        print("Такого автора немає!")
    else:
        lib.pop(author_name)
        author_name = input("Введіть нове ім'я для автора: ")
        lib[author_name] = books_list
        print("Зміни внесені!")


def change_writings(lib):
    for k, v in lib.items():
        print_names(k, lib)
    print("Введіть автора, твір якого треба змінити: ")
    author_name = input(">> ")
    books_list = lib.get(author_name)
    if not books_list:
        print("Такого автора немає!")
    else:
        print("Яку з робіт ви хочете змінити: ")
        for i, val in enumerate(books_list):
            print(f"{i} - {val}")
        book_ind = int(input("Номер роботи: "))
        writing_name = input("Нова назва для роботи: ")
        books_list[book_ind] = writing_name
        print("Переписано!")


question_string = "Чого бажаєте?\n1. Зробити запис\n2. Вивести в відсортованому вигляді\n3. Змінити запис\n4. Вийти"
library = defaultdict(list)
library['Bertrand Russell'].extend(["History of Western Philosophy", "The Principles of Mathematics",
                                    "Human Society in Ethics and Politics"])
library['George Orwell'].extend(["A Little Poem", "On a Ruined Farm Near the His Master's Voice Gramophone Factory",
                                 "Sometimes in the Middle Autumn Days"])
library['Howard Phillips Lovecraft'].extend(["Old Bugs", "The Street", "The Other Gods", "Dagon",
                                             "The Shadow over Innsmouth"])

while True:
    ui = get_ui_val(question_string, 4)
    match ui:
        case "1":
            authors_name = input("Введіть ім'я автора: ")
            book_name = input("Введіть назву роботи: ")
            library[authors_name].append(book_name)
            print("Записано!")
            print()
        case "2":
            sorting_questions_string = "Як бажаєте відсортувати?" \
                                       "\n1. Відсортувати за автором" \
                                       "\n2. Відсортувати за твором" \
                                       "\n3. Вивести в поточному сортуванні"

            sort_variant = get_ui_val(sorting_questions_string, 3)
            sort_and_print_vals(library, sort_variant)
        case "3":
            changing = get_ui_val("Ви бажаєте: \n1. Змінити автора\n2. Змінити твори\n3. Нічого не змінювати", 3)
            match changing:
                case "1":
                    change_author(library)
                case "2":
                    change_writings(library)
        case "4":
            print("До побачення!")
            break
