from task7_2 import get_ui_val
"""
Завдання 7

Створіть прототип програми «Облік кадрів», в якій є можливість
перегляду та внесення змін до структури
(реалізуйте інтерфейс(меню), за допомогою якого можна робити
маніпуляції з даними):

прізвище:

    посада: ...

    досвід роботи: …

    портфоліо: …

    коефіцієнт ефективності: …

    стек технологій: …

    зарплата: …

Передбачте можливість виведення на екран сортування
за прізвищем та найефективнішим співробітником.
"""

personnel_records = []
personnel_records.append({'surname': 'Walker',
                          'job_title': 'designer',
                          'experience': 10,
                          'portfolio': 9,
                          'efficiency_ratio': 77,
                          'technology_stack': 'Photoshop, Illustrator',
                          'salary': 4500})
personnel_records.append({'surname': 'Wayne',
                          'job_title': 'programmer',
                          'experience': 2,
                          'portfolio': 8,
                          'efficiency_ratio': 91,
                          'technology_stack': 'Django, Sql',
                          'salary': 6300})
personnel_records.append({'surname': 'Alexander',
                          'job_title': 'cleaner',
                          'experience': 1,
                          'portfolio': 100,
                          'efficiency_ratio': 4,
                          'technology_stack': 'No',
                          'salary': 2000})

aliases = \
    {"1": ['surname', 'Прізвище'],
     "2": ["job_title", "Посаду"],
     "3": ["experience", "Досвід роботи"],
     "4": ["portfolio", "Портфоліо"],
     "5": ["efficiency_ratio", "Коефіцієнт ефективності"],
     "6": ["technology_stack", "Стек технологій"],
     "7": ["salary", "Зарплату"]}


def add_new_record(records):
    print("Введіть прізвище: ")
    surname = input(">> ")
    print("Введіть посаду: ")
    job_title = input(">> ")
    print("Введіть досвід роботи(повних років): ")
    experience = input(">> ")
    print("Введіть портфоліо: ")
    portfolio = input(">> ")
    print("Введіть коефіцієнт ефективності (int): ")
    efficiency_ratio = int(input(">> "))
    print("Введіть стек технологій: ")
    technology_stack = input(">> ")
    print("Введіть зарплату: ")
    salary: str = input(">> ")

    vals = {'surname': surname,
            'job_title': job_title,
            'experience': experience,
            'portfolio': portfolio,
            'efficiency_ratio': efficiency_ratio,
            'technology_stack': technology_stack,
            'salary': salary}
    records.append(vals)
    print("Записано")


def change_record(records):
    rewrite = False
    question = \
        "Що б ви хотіли змінити?" \
        "\n1. Прізвище" \
        "\n2. Посаду" \
        "\n3. Досвід роботи(повних років)" \
        "\n4. Портфоліо" \
        "\n5. Коефіцієнт ефективності" \
        "\n6. Стек технологій" \
        "\n7. Зарплату"

    for i in records:
        for k, v in i.items():
            print(f"{k}: {v}")
        print()

    surname = input("Введіть прізвище співробітника: ")
    for i in records:
        if i['surname'] == surname:
            ans = get_ui_val(question, 7)
            key = aliases[ans][0]
            print(f"Введіть нову / новий {aliases[ans][1]}")
            value = input(">> ")
            i[key] = value
            rewrite = True

    if rewrite:
        print("Записано")
    else:
        print("Запис по такому прізвищу не знайдено!")


def delete_record(records):
    for ind, val in enumerate(personnel_records):
        print(f"{'*' * 5} {ind} {'*' * 5}")
        for k, v in val.items():
            print(f"{k}: {v}")
        print()

    print("введіть номер запису для видалення: ")
    num = int(input(">> "))
    if 0 < num < len(records):
        records.pop(num)
        print('Видалено!')
    else:
        print("Некорректний індекс")


def sort_records(records, sort_variant):
    if sort_variant == '1':
        print("За прізвищем: ")
        print()

        for i in sorted(records, key=lambda x: x['surname']):
            for k, v in i.items():
                print(f"{k}: {v}")
            print()

    elif sort_variant == "2":
        print("За ефективністю: ")
        print()
        for i in sorted(records, key=lambda x: x['efficiency_ratio'], reverse=True):
            for k, v in i.items():
                print(f"{k}: {v}")
            print()
    else:
        for i in records:
            for k, v in i.items():
                print(f"{k}: {v}")
            print()


question_string = 'Чого бажаєте?\n1. Внести зміни\n2. Відсортувати\n3. Вийти'
while True:
    user_choice = get_ui_val(question_string, 3)
    match user_choice:
        case "1":
            q_1_var = 'Ви бажаєте:\n1. Додати новий запис\n2. Змінити існуючий\n3. Видалити існуючий\n4. Вийти'
            change_variant = get_ui_val(q_1_var, 4)
            match change_variant:
                case '1':
                    add_new_record(personnel_records)
                case '2':
                    change_record(personnel_records)
                case '3':
                    delete_record(personnel_records)
        case "2":
            q_2_var = "Ви бажаєте відсортувати за:" \
                      "\n1. Прізвищем" \
                      "\n2. За ефективністю" \
                      "\n3. Поточне сортування"
            sort_variant = get_ui_val(q_2_var, 3)
            sort_records(personnel_records, sort_variant)
        case "3":
            print("До побачення!")
            break
