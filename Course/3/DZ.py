from math import pi, cos, sqrt, sin, tan, radians

"""
Завдання 1
"""

name = input("Введіть ваше ім'я: ")
print(f"Вітаю, {name}!")

if name == "Єгор":
    print("Мене також звати Єгор!")
else:
    print("А мене звати Єгор")

"""
Завдання 2
"""

x = float(input("Введіть x: "))
if -pi <= x <= pi:
    print(f"y = {cos(3*x)}")
else:
    print("x має бути в проміжку -pi <= x <= pi")


"""
Завдання 3
"""

a, b, c = float(input('Введіть a: ')), float(input('Введіть b: ')), float(input('Введіть c: '))

d = (b**2)-(4*a*c)
print(d)

if d == 0:
    x = -b/(2*a)
    print(f"x = {x}")
elif d < 0:
    print("Коренів немає")
else:
    x1 = ((-b + d**0.5) / (2*a))
    x2 = ((-b - d**0.5) / (2*a))
    print(f"x1 = {x1}, x2 = {x2}")

"""
Завдання 4
"""

operators_1 = ("+", "-", "*", "/", "^")
operators_2 = ("2корінь", "3корінь", "sin", "cos", "tan")

print('Введіть один з операторів: +, -, *, /, ^, 2корінь, 3корінь, sin, cos, tan')
operator = input("Введіть оператор: ")

if operator in operators_1:
    operand1 = float(input("Введіть 1 число: "))
    operand2 = float(input("Введіть 2 число: "))

    if operator == "+":
        print(f"{operand1} + {operand2} = {operand1 + operand2}")
    elif operator == "-":
        print(f"{operand1} - {operand2} = {operand1 - operand2}")
    elif operator == "*":
        print(f"{operand1} * {operand2} = {operand1 * operand2}")
    elif operator == "/":
        if operand2 != 0:
            print(f"{operand1} / {operand2} = {operand1 / operand2}")
        else:
            print("На 0 ділять!")
    elif operator == "^":
        print(f"{operand1} ^ {operand2} = {operand1 ** operand2}")

elif operator in operators_2:
    operand = float(input("Введіть число: "))

    if operator == "2корінь":
        print(f"2корінь({operand}) = {sqrt(operand)}")
    elif operator == "3корінь":
        if operand < 0:
            operand = abs(operand)
            print(f"3корінь({operand}) = {operand ** (1 / 3) * (-1)}")
        else:
            print(f"3корінь({operand}) = {operand**(1/3)}")

    elif operator == "sin":
        print(f"sin({operand})degrees = {sin(radians(operand))}, sin({operand})radians = {sin(operand)}")
    elif operator == "cos":
        print(f"cos({operand})degrees = {cos(radians(operand))}, cos({operand})radians = {cos(operand)}")
    elif operator == 'tan':
        print(f"tan({operand})degrees = {tan(radians(operand))}, tan({operand})radians = {tan(operand)}")
else:
    print("Невідомий оператор")

"""
Завдання 5
"""
number = int(input("Введіть число: "))
print("непарне" if number % 2 else "парне")

"""
Завдвння 6
"""
print('Введіть один із днів: Пн, Вт, Ср, Чт, Пт, Сб, Нд')
working_days = ("Пн", "Вт", "Ср", "Чт", "Пт")
non_working_days = ("Сб", "Нд")
day = input("День тижня: ")

if day in working_days:
    print("Сьогодні на роботу")
elif day in non_working_days:
    print("Сьогодні вихідний")
else:
    print("Такого дня не існує")

# print("Сьогодні на роботу" if day in working_days else "Сьогодні вихідний" if \
#       day in non_working_days else "Такого дня не існує")
