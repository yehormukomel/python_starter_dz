# Завдання 4

from task_3 import get_user_choice, my_isdigit


# Створіть магазин канцтоварів використовуючи списки для зберігання елементів.
# Для додавання елементів створіть функцію, яка буде запитувати дані в
# користувача і зібрані дані у вигляді кортежу додавати у створений
# список на початку. Результат вивести на екран. Під час створення
# дотримуйтесь правил специфікації PEP 8.


def add_new_product(products_list: list):
    product_name = input("Введіть назву товара: ")
    price = input("Введіть ціну товара: ")
    if not my_isdigit(price):
        print("Ціна - це число!")
        return
    print("Записано!")
    print()
    products_list.insert(0, (product_name, price))


def print_products(products_list: list):
    for price_name, price in products_list:
        print(f"Назва товару: {price_name}\nЦіна: {price}")
        print()


def main():
    products = []
    products.extend([("Папка-регистратор Buromax А4 50 мм PP Темно-Синяя", 116),
                     ("Записная книга Moleskine Classic 19 х 25 см 192 страницы в линейку Черная", 895),
                     ("Ручка шариковая Parker Jotter 17 SS CT BP Синяя Серебристый корпус", 1064)])

    question_string = "Чого бажаєте?" \
                      "\n1. Додати новий товар" \
                      "\n2. Вивести на екран всі товари" \
                      "\n3. Вийти"
    while True:
        print(question_string)
        user_choice = get_user_choice(question_string, parse_string=True)
        match user_choice:
            case "1":
                add_new_product(products)
            case "2":
                print_products(products)
            case "3":
                print("До побачення!")
                return


if __name__ == '__main__':
    main()
