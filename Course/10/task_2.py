import math

# Завдання 2
#
# Відкрийте файл fix_me.py із папки homework.
# Використовуючи звичайний текстовий редактор (Notepad),
# виправте всі помилки оформлення коду згідно з PEP 8.

# Я й назви трохи виправив

print_count = 5
repeat_count = int(input('Введите количество повторов: '))
print(print_count * repeat_count)
print(math.pi * print_count * repeat_count)
print(math.e * 2)

while print_count >= 0:
    print_count -= 1

user_string = 'my string'
sum_of_degrees = 0
for elem in user_string:
    sum_of_degrees += pow(user_string.find(elem), 2)
    print("sum=", sum_of_degrees)


def my_func(atr=1):
    print('atr', atr)


my_func(atr=5)
# print(my_func(atr=5))
