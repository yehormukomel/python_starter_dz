import math


# Завдання 3
#
# Створіть інженерний калькулятор з використанням модуля math,
# в якому передбачене меню. Під час створення дотримуйтесь
# правил специфікації PEP 8.


def my_isdigit(s):
    return s.replace('.', '', 1).isdigit()


def get_user_choice(q_string: str = None,
                    parse_string: bool = False,
                    var_count: int = 0) -> str:
    if parse_string:
        var_count = max([int(i) for i in q_string if i.isdigit()])
    variants = [str(i) for i in range(1, var_count + 1)]
    user_input = input(">> ")
    while user_input not in variants:
        print("Потрібно ", end='')
        print(*variants, sep=' або ', end='!')
        print()
        user_input = input(">> ")
    return user_input


def get_operands(operands_count=1):
    if operands_count == 1:
        operand = input("Введіть операнд: ")
        while not my_isdigit(operand):
            print("Потрібно ввести число!")
            operand = input("Введіть операнд: ")
        return float(operand)

    elif operands_count == 2:
        operand_1 = input("Введіть операнд 1: ")
        operand_2 = input("Введіть операнд 2: ")
        while not all([my_isdigit(operand_1), my_isdigit(operand_2)]):
            print("Потрібно ввести 2 числа: ")
            operand_1 = input("Введіть операнд 1: ")
            operand_2 = input("Введіть операнд 2: ")
        return float(operand_1), float(operand_2)


def print_operations(operations: dict):
    print("Оберіть потрібну операцію: ")
    for index, element in operations.items():
        print(f"{index}: {element[0]}")


def calculate_result(operand_1, operation_item, operand_2=None):
    symbol = operation_item[0]
    operation = operation_item[1]
    if operand_2 == 0 and symbol == '/':
        print("На 0 не ділять!")
        return False
    if operand_1 and operand_2:
        return operation(operand_1, operand_2)
    else:
        return operation(operand_1)


def print_result(operand_1, result, symbol, operand_2=None):
    if operand_1 and operand_2:
        print(f"{operand_1} {symbol} {operand_2} = {result}")
    else:
        print(f"{symbol}({operand_1}) = {result}")


def main():
    operations = {1: ('+', lambda x, y: x + y),
                  2: ('-', lambda x, y: x - y),
                  3: ('*', lambda x, y: x * y),
                  4: ('/', lambda x, y: x / y if y else "На 0 не ділять!"),
                  5: ('^', lambda x, y: x ** y),
                  6: ('sin', lambda x: round(math.sin(math.radians(x)), 3)),
                  7: ('cos', lambda x: round(math.cos(math.radians(x)), 3)),
                  8: ('tan', lambda x: round(math.tan(math.radians(x)), 3)),
                  9: ('sqrt', math.sqrt),
                  10: ('logn', math.log),
                  11: ('Вийти', None)}

    operand_1 = operand_2 = previous_value = None
    while True:
        print_operations(operations)
        user_choice = int(get_user_choice(var_count=len(operations)))
        operation = operations[user_choice]
        operation_symbol = operation[0]

        if 0 < user_choice < 6:
            if previous_value:
                question_string = "Який з операндів ви б хотіли замінити" \
                                  " на результат минуголо обчислення?" \
                                  "\n1. Перший\n2. Другий"
                print(question_string)
                user_choice = get_user_choice(question_string,
                                              parse_string=True)
                if user_choice == '1':
                    operand_1 = previous_value
                    operand_2 = get_operands()
                elif user_choice == '2':
                    operand_1 = get_operands()
                    operand_2 = previous_value
            else:
                operand_1, operand_2 = get_operands(2)
            result = calculate_result(operand_1=operand_1,
                                      operand_2=operand_2,
                                      operation_item=operation)

            if not result:
                print("Помилка в обчисленнях!")
                continue
            else:
                print_result(operand_1=operand_1,
                             operand_2=operand_2,
                             result=result,
                             symbol=operation_symbol)

        elif user_choice < 11:
            if previous_value:
                operand = previous_value
            else:
                operand = get_operands()

            result = calculate_result(operand_1=operand,
                                      operation_item=operation)
            print_result(operand_1=operand,
                         result=result,
                         symbol=operation_symbol)
        else:
            print("До побачення!")
            break

        question_string = "Бажаєте використати поточне" \
                          " значення в наступному обчисленні?" \
                          "\n1. Так\n2. Ні"
        print(question_string)
        user_choice = get_user_choice(question_string, parse_string=True)
        if user_choice == "1":
            previous_value = result
        else:
            previous_value = None


if __name__ == '__main__':
    main()
