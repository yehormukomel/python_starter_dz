from collections import namedtuple, deque
"""
Завдання 1
"""

full_name = input("Напишіть Ваше Прізвище, Ім'я, По батькові (все через пробіл): ").split()


if len(full_name) != 3 or not all([i.isalpha() for i in full_name]):
    print("Будь ласка введіть правильно!")
else:
    surname, name, second_name = [i.capitalize() for i in full_name]
    print(f"Ви ввели: Прізвище: {surname} Ім'я: {name} По батькові: {second_name}")


"""
# Завдання 2
"""

nums = input("Введіть діапазон чисел (не менше 5): ").split()

if len(nums) < 5:
    print("Тут менше 5 чисел, а має бути хоча б 5")
else:
    nums = [int(i) for i in nums]
    avg = sum(nums)/len(nums)
    sum_of_nums = sum((nums[1], nums[-2], avg))
    print(sum_of_nums)

"""
# Завдання 3
"""

r, g, b = int(input("R: ")), int(input("G: ")), int(input("B: "))

if all(256 > i > 0 for i in (r, g, b)):
    color = (r, g, b)
    print(f"Color: {color}")
else:
    print("Incorrect color!")


"""
# Завдання 4
"""

params = ["student_name", 'Algebra', 'Geometry', 'History', 'Informatics', 'Geography']
Marks = namedtuple('Marks', params)
all_students = deque()
all_students.append(Marks(student_name='John Wayne', Algebra=88, Geography=99, History=77,
                          Informatics=100, Geometry=100))
all_students.append(Marks(student_name='Paul Walker', Algebra=99, Geometry=99, History=77,
                          Informatics=100, Geography=56))

print(all_students)

"""
# Завдання 5
"""
print("Введіть послідовність чисел через пробіл: ")
ui = tuple(sorted(input(">> ").split()))
print(ui)

"""
# Завдання 6
# 
# Прошу,поправте, якщо я не правильно зрозумів умову задачі
"""

mentions = {"меню": 0, "спортзал": 0, "обслуговування": 0}
ui = input("Ваш фідбек: ")
ui_list = ui.split()

for i in ui_list:
    i = i.lower()
    if i in mentions.keys():
        mentions[i] += 1

res = f"""
Згадок про меню: {mentions['меню']}
Згадок про спортзал: {mentions['спортзал']}
Згадок про обслуговування: {mentions['обслуговування']}
"""
res += 'Надана знижка 15%' if len(ui) > 60 else ''
print(res)
