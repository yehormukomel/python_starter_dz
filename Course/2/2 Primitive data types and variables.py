# strings

# name_1 = 'Це наша перший рядок'
# name_2 = "Це наша другий рядок"
# name_3 = 'Це наш "перший" рядок'
# name_4 = "Це наш 'перший' рядок"
#
# empty = ''
#
# error_1 = "перше, друге, \"трєтє\""
# print(error_1)
# print("\\")
#
# print("друкуємо табуляцію - d\td")
#
# name = input("Введіть ім'я: ")
# print(f"Ваше ім'я: {name}")

# print("123456789"[8])
# raw_str = r"ceieffrf \ frfyrgfrufe"

# a = 'dddd' + 'ffff'
# print(a)
# print("--"*80)

# d = "Ще не вмерла України ні слава ні воля"
# result = "Ще не " in d
# print(result)

# bool
# Методы строк

# s = 'Мама мыла раму'
#
# print(s.upper())

# numbers
