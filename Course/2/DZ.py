"""
Завдання 1
"""

word1 = input('Введіть 1 слово: ')
word2 = input('Введіть 2 слово: ')
print(f'{word1}, {word2}')


"""
Завдання 2 
"""

a, b, x = int(input('Введіть ціле чило a: ')), int(input('Введіть ціле чило b: ')), int(input('Введіть ціле чило x: '))
print(f'Добуток: {a*b*x}')

"""
Завдання 3
"""

a, b, c = float(input('Введіть a: ')), float(input('Введіть b: ')), float(input('Введіть c: '))

d = (b**2)-(4*a*c)

if d == 0:
    x = -b/(2*a)
    print(f"x = {x}")
else:
    x1 = ((-b + d**0.5) / (2*a))
    x2 = ((-b - d**0.5) / (2*a))
    print(f"x1 = {x1}, x2 = {x2}")


"""
Завдання 4
"""
ui = input("Введіть фразу з клавіатури: ")
print(sum([ord(i) for i in ui]))
# Як це зробити без циклу взагалі?



"""
Завдання 5
"""
ui = input('Введіть текст: ')
print(ui[::-1])

"""
Завдання 6
"""
radius = float(input("Введіть радіус: "))
print(f"S = {3.141592653589793 * (radius**2)}")

"""
Завдання 7 
"""
length = 700
velocity = 90
time = length/velocity

print(time)

"""
Завдання 8
"""
name = input("Введіть ім'я: ")
age = int(input("Введіть вік: "))

print("My name is " + str(name) + " and I am " + str(age))

# age потрібно перетворювати в int в будь-якому разі?
# Чи правильно я зрозумів умову?