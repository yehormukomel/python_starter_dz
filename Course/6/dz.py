"""
Завдання 1
"""
ui = input("Введіть значення списку: ").split()
ui = [int(i) for i in ui]

min_elem = min(ui)
max_elem = max(ui)
sum_of_list = sum(ui)
avg_of_list = sum_of_list/len(ui)

res = f"""
min: {min_elem}
max: {max_elem}
sum: {sum_of_list}
avg: {avg_of_list}
"""
print(res)

"""
Завдання 2
"""
ui1 = set(input("Введіть значення для 1 списку: ").split())
ui2 = set(input("введіть значення для 2 списку: ").split())
value_set = ui1.symmetric_difference(ui2)

if all(i.isdigit() for i in value_set):
    res_list = [int(i) for i in value_set]
else:
    res_list = list(value_set)

print(f"В прямій послідовності: {res_list}")
res_list.reverse()
print(f"В зворотній послідовності: {res_list}")
res_list.sort()
print(f"Сортування за зростанням: {res_list}")
res_list.sort(reverse=True)
print(f"Сортування за спаданням: {res_list}")

"""
Завдання 3
"""
start, end = int(input("Введіть start: ")), int(input("Введіть end: "))
simple_nums = []
for number in range(start, end + 1):
    if number > 1:
        for i in range(2, number):
            if number % i == 0:
                break
        else:
            simple_nums.append(number)

print("Прості числа: ", end='')
for i in simple_nums:
    print(i, end=' ')
print("\n")

while True:
    print("Бажаєте вивести сумму чисел або добуток?")
    print("Виберіть один із варіантів: \n1. Вивести сумму\n2. Вивести добуток\n3. Вийти")
    ui = input("1 або 2 або 3: ")

    match ui:
        case '1':
            print(f"Сума: {sum(simple_nums)}")
            break
        case '2':
            prod = 1
            for x in simple_nums:
                prod *= x
            print(f"Добуток: {prod}")
            break
        case '3':
            print("ok")
            break
        case _:
            print("Потрібно 1 або 2 або 3!")
            print()


"""Завдання 4"""

count_of_list = int(input("Введіть кількість значень: "))
elements_list = []

for i in range(1, count_of_list+1):
    print(f"введіть елемент {i}")
    ui = input(">> ")
    elements_list.append(ui)

if all([i.isdigit() for i in elements_list]):
    elements_list = [int(i) for i in elements_list]

print("Введіть 1 щоб вивести значення у зворотному порядку")
print("Введіть 2 щоб вивести значення за зростанням")

while True:
    ui = input("(1 або 2)>> ")
    if ui not in ("1", "2"):
        print("Необхідно ввести 1 або 2!")
        continue
    else:
        break

if ui == '1':
    elements_list.sort(reverse=True)
    print(f"У зворотному порядку: {elements_list}")
elif ui == "2":
    elements_list.sort()
    print(f"За зростанням: {elements_list}")


"""
Завдання 5
"""
int_list = input("Введіть декілька натуральних чисел: ").split()
int_list = [int(i) for i in int_list]
new_list = []
repeat_count = int(input("Введіть кількість повторів: "))

for i in int_list:
    if i % 2:
        new_list.append(i)

new_list *= repeat_count
int_list.clear()


"""
Завдання 6
"""
x = int(input("Введіть значення: "))
if x in new_list:
    print(f"Кількість повторів: {new_list.count(x)}")
    print(f"Позиція у цьому списку: {new_list.index(x)}")

