from prettytable import PrettyTable

"""
Завдання 1

Створіть функцію, яка відображає привітання для користувача
із заданим ім'ям. Якщо ім'я не вказано, вона повинна
виводити привітання для користувача з Вашим ім'ям.
"""


def greeting(name='Yehor'):
    print(f"Hello, {name}!")


# greeting('John')
# greeting('Vovan')


"""
Завдання 2

Створіть дві функції, що обчислюють значення
певних алгебраїчних виразів. На екрані
виведіть таблицю значень цих функцій від -5 до 5 з кроком 0.5.
"""


# def my_range(start, stop=None, step=None):
#     start = float(start)
#     if stop is None:
#         stop = start + 0.0
#         start = 0.0
#     if step is None:
#         step = 1.0
#
#     count = 0
#     while True:
#         temp = float(start + count * step)
#         if step > 0 and temp >= stop:
#             break
#         elif step < 0 and temp <= stop:
#             break
#         yield temp
#         count += 1


def square_root(num):
    return num ** 0.5


def cube_root(num):
    if num < 0:
        num = abs(num)
        return num ** (1 / 3) * (-1)
    else:
        return num ** (1 / 3)


def task_2():
    table = PrettyTable()
    table.field_names = ['number', 'square root', 'cube root']

    # for i in my_range(-5, 5.5, 0.5):
    #     table.add_row([i, square_root(i), cube_root(i)])

    i = -5.5
    while i < 5.5:
        i += 0.5
        table.add_row([i, square_root(i), cube_root(i)])

    print(table)


task_2()

"""
Завдання 3

Створіть програму-калькулятор, яка підтримує наступні операції:
додавання, віднімання, множення, ділення, зведення в ступінь,
зведення до квадратного та кубічного коренів. Всі дані повинні
вводитися в циклі, доки користувач не вкаже, що хоче завершити
виконання програми. Кожна операція має бути реалізована у 
вигляді окремої функції. Функція ділення повинна перевіряти
дані на коректність та видавати повідомлення про помилку у
разі спроби поділу на нуль.
"""


def my_isdigit(s):
    return s.replace('.', '', 1).isdigit()


operations1 = {'+': lambda x, y: x + y,
               '-': lambda x, y: x - y,
               '*': lambda x, y: x * y,
               '/': lambda x, y: x / y if y else "На 0 не ділять!",
               '^': lambda x, y: x ** y}

operations2 = {'square_root': square_root, 'cube_root': cube_root}

all_operators = list(operations1.keys())
all_operators.extend(list(operations2.keys()))

# while True:
#     print('Надрукуйте бажану операцію: ')
#     for operation in all_operators:
#         print(operation, end='; ')
#     print()
#     print("Щоб вийти надрукуйте: stop")
#     ui = input('>> ')
#
#     if ui in operations1:
#         operand1 = input("Введіть 1 операнд: ")
#         operand2 = input("Введіть 2 операнд: ")
#         if all([my_isdigit(operand1), my_isdigit(operand2)]):
#             operand1 = float(operand1) if not operand1.isdigit() else int(operand1)
#             operand2 = float(operand2) if not operand2.isdigit() else int(operand2)
#             print(operations1[ui](operand1, operand2))
#         else:
#             print("Потрібні тільки числа!")
#     elif ui in operations2:
#         operand = input("Введіть операнд: ")
#         if my_isdigit(operand):
#             operand = float(operand) if not operand.isdigit() else int(operand)
#             print(operations2[ui](operand))
#         else:
#             print("Потрібні тільки числа!")
#     elif ui.lower() == 'stop':
#         print("До побачення!")
#         break

"""
Завдання 4

Створіть програму, яка складається з функції,
яка приймає три числа і повертає їх середнє
арифметичне, і головного циклу, що запитує у
користувача числа і обчислює їх середні 
значення за допомогою створеної функції. 
"""


def my_avg(values):
    return sum(values) / len(values)


# while True:
#     print("Якщо бажаєте вийти введіть: stop")
#     ui = input("Введіть 3 числа(через пробіл): ")
#     if ui.lower() == 'stop':
#         print("До побачення!")
#         break
#     ui = ui.split()
#     if len(ui) != 3:
#         print("Потрібно 3 числа!")
#     else:
#         if all([my_isdigit(i) for i in ui]):
#             ui = [float(i) for i in ui]
#             print(f"Avg: {my_avg(ui)}")
#         else:
#             print("Потрібні числа!")

"""
Завдання 5

Створіть програму, яка приймає як формальні параметри зріст
і вагу користувача, обчислює індекс маси тіла і в залежності
від результату повертає інформаційне повідомлення
(маса тіла в нормі, недостатня вага або слідкуйте за фігурою).
Користувач з клавіатури вводить значення росту та маси тіла та
передає ці дані у вигляді фактичних параметрів під час
виклику функції. Програма працює доти, доки користувач
не зупинить її комбінацією символів «off».
"""


def get_bmi(height, weight):
    BMI = weight / (height / 100) ** 2
    print(f"Ваш BMI: {BMI}")
    if BMI <= 18.4:
        print("Недостатня вага.")
    elif BMI <= 24.9:
        print("Маса тіла в нормі.")
    else:
        print("Слідкуйте за фігурою.")


# while True:
#     print("Якщо бажаєте вийти, просто введіть: off")
#     height = input("Ваш зріст (см): ")
#     if height.lower() == 'off':
#         print('До побачення!')
#         break
#     weight = input("Ваша вага (кг): ")
#     if weight.lower() == 'off':
#         print('До побачення!')
#         break
#
#     height, weight = float(height), float(weight)
#     get_bmi(height, weight)
#     print()
